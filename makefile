target="dist/book"
options="--table-of-contents"
clean:
	rm -rf dist/*
prepare:
	if [ ! -d $(target) ]; then mkdir $(target); fi
pdf: prepare
	pandoc -o $(target).pdf metadata.txt src/*.md $(options) -t html
epub: prepare
	pandoc -o $(target).epub metadata.txt src/*.md $(options)
html: prepare
	pandoc -o $(target).html metadata.txt src/*.md $(options)
plain: prepare
	pandoc -o $(target).txt metadata.txt src/*.md
_all: pdf epub html plain

all:
	$(MAKE) clean; $(MAKE) _all
