# Progetto RATTO

Come i miei ratti domestici, gli vorrò un sacco bene e mi tirerà cacca.

## Howto

### Prerequisiti
1. [pandoc](https://pandoc.org/installing.html)
2. [whtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf) (per ora...)

Ogni file dentro `src` è un capitolo. Credo che vengano letti in ordine alfabetico, quindi la convenzione è di mettere il numero in cima.

Si scrive in [Markdown](https://daringfireball.net/projects/markdown/), si compila con [make](https://www.gnu.org/software/make/). Output supportati:
1. PDF
2. HTML
3. Epub (rotto)

## Roadmap
* [ ] Metterci dentro della roba vera
* [ ] Riparare gli Epub
* [ ] CI per le build automatiche
* [ ] Un layout decente?
