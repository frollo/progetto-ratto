# Armi e combattimento

## Combattimento

Il combattimento non utilizza una griglia di posizionamento, ma una soluzione più approssimativa (a meno che non vogliate una griglia di combattimento - funziona uguale). In ordine di iniziativa (vedi sotto, alla voce "Iniziativa"), i giocatori dichiarano l'azione, tirano eventuali dadi e, nel caso in cui i colpi vadano a segno, infliggono i danni. Ogni turno dura circa 5 secondi e le azioni avvengono quasi in contemporanea (ogni combattente inizia la propria azione più o meno mentre il combattente precedente - o l'ultimo del turno prima - sta terminando la propria).

Quando faccio riferimento alla griglia, assumo che ogni riquadro abbia 1,5m di lato.

### Azioni in un round di combattimento

Ogni personaggio, all'interno del proprio round di combattimento, può svolgere due delle seguenti azioni:

* muoversi
* usare un oggetto (pozioni, pergamene, etc sono tutti oggetti, anche usare un'arma conta come "usare un oggetto")
* prendere un oggetto a portata di mano (un'arma infoderata, una pergamena portata alla cintura...)

Oppure può svolgere _una sola_ delle seguenti azioni:

* lanciare un incantesimo
* prendere un oggetto dallo zaino

Attaccare un avversario _minacciato_ (vedi sotto) con l'arma con cui lo state minacciando è sempre una sola azione, anche se prevede un passo.

Attaccare un avversario è una prova di abilità come un'altra (vedi "Prove di abilità"), in cui l'attaccante tira Forza + Combattimento per le armi da mischia e Destrezza + Combattimento per le armi a distanza. Per facilitare i conti, la difficoltà è pre-calcolata e annotata come Classe Armatura (CA). Per calcolare la CA si sommano: 10 + Destrezza + Eventuali bonus (armatura, scudo, incantesimi di protezione...). In caso di successo, si tira il dado di danno.

## Iniziativa

L'ordine di iniziativa si determina tirando Percezione + Combattimento, dal risultato più alto al più basso. L'ordine rimane lo stesso finché uno o più combattenti non si aggiungono (arrivano, vengono evocati, si rialzano da ferite prima invalidanti...) o si levano (brutte fini, fughe...) dal combattimento. A quel punto si tira di nuovo e si determina il nuovo ordine

### Un po' di gergo

Come dicono a Roma, famo a capisse:

* _minacciare_: Alice _minaccia_ Bob se le è sufficiente fare un singolo passo e stendere il braccio per colpirlo con la propria arma.

### Distanze in combattimento

Le distanze in combattimento sono sempre relative a un avversario e sono fondamentalmente quattro:

  1. Gioco stretto: i due combattenti sono uno addossato all'altro (se usate una griglia, occupano lo stesso quadretto); la maggior parte delle armi è pressoché inutile a questa distanza, è il momento di calci, pugni e morsi alle orecchie.
  2. Gioco medio: per colpire l'avversario con la maggior parte delle armi è sufficiente allungare il braccio (se usate una griglia, sono in due quadretti adiacenti); è la distanza da cui la maggior parte delle armi da mischia comincia a funzionare.
  3. Gioco largo: i due combattenti sono a un passo di distanza (se usate una griglia, c'è un quadretto vuoto tra i due); è la distanza da cui la maggior parte delle armi da mischia _minaccia_.
  4. Disingaggiato: i due combattenti sono a due o più passi di distanza; se volete colpire qualcuno con la vostra spada a questa distanza, dovete svitare il pomolo e lanciarglielo in faccia, "per finirlo per bene".

## Armi

Tutte le armi infliggono 1d6 danni, quando possono essere usate. Se un personaggio si trova in gioco stretto e non ha un'arma che possa usare a quella distanza può usare impugnature, pomoli, mani, testa, ginocchia e quant'altro, ma solo per 1d4 danni a colpo.

### Armi da mischia

| Tipo di arma | Gioco stretto | Gioco medio | Gioco largo |
|:---|:---:|:---:|:---:|
| Pugnali e daghe | 1d6 | minaccia | X |
| Asce | 1d4 | 1d6 | minaccia |
| Spade | 1d6 | 1d6 | minaccia |
| Armi in asta | 1d4 | 1d6 | 1d6 |
| Martelli | 1d4 | 1d6 | minaccia |
| Mazze | 1d4 | 1d6 | minaccia |
| Bastoni | 1d4/1d6 | 1d6 | minaccia |

#### Pugnali e daghe

Armi corte, adatte alla distanza ravvicinata, molto meno efficaci se ci si trova a dover affrontare un avversario dotato di un'arma più lunga. Le loro dimensioni ridotte li rendono più semplici da nascondere (+2 alle prove per occultarli) e possono essere usati come seconda arma.

#### Asce

Sono uno strumento da falegname convertito in arma. Le asce da battaglia sono pensate più per fare a pezzi la gente che non i pezzi di legno, ma funzionano comunque. Contano come strumento del mestiere quando si parla di falegnameria.

#### Spade

L'arma dei nobili. In mano a un professionista sono armi temibili, in grado sia di offendere che di difendere. I Combattenti che impugnano una spada hanno +1 alla CA.

#### Armi in asta

Picche, lance, alabarde e simili. Fondamentalmente un sacco di roba affilata in cima a un bastone lungo. Possono colpire da molto più lontano delle altre armi e sono ottimi per tenere a distanza cavalieri e persone poco raccomandabili.

#### Martelli

Un altro strumento da lavoro convertito in arma. Sono pensati per sfondare e questo li rende molto efficaci quando si parla di avversari in armatura: ignorano 2 punti di CA dalle armature (ma non dagli scudi). Usare un martello per sfondare casse, porte e simili da +2 alle prove di forza.

#### Mazze

Sono la versione di metallo delle clave, magari con degli spuntoni sopra. Questa categoria comprende anche i mazzafrusti, se proprio vi piace quell'obbrobrio. Come i martelli, ignorano 2 punti di CA dalle armature (ma non dagli scudi), ma sono meno adatte ai lavori manuali, quindi non danno bonus di sorta alle prove di forza.

#### Bastoni

Solidi pezzi di legno da dare in testa alla gente. I Maghi si sono abituati a incantarli da tempo e hanno sviluppato formule e incantesimi dedicati, cosa che gli dà un +2 alle prove di Intelligenza per farne oggetti magici (vedi il capitolo "Magia e Oggetti Magici"). In mano a qualcuno di allenato, possono fare molto male anche quando non c'è spazio di rotearli decentemente, per questo Combattenti e Maghi (che sono abituati ad averne sempre uno con sè) possono usarli per infliggere 1d6 danni, invece che 1d4, anche in Gioco Stretto.

### Armi a distanza

Le armi a distanza scagliano proiettili di diverso tipo verso il nemico. Non possono essere usate in gioco stretto, raramente in gioco medio e iniziano a diventare utili quando si parla di gioco largo. Spesso richiedono di essere ricaricate, cosa che richiede un certo numero di azioni. Quando un'arma richiede 0 azioni per essere ricaricata, significa che viene ricaricata nella stessa azione in cui viene usata.

| Tipo di arma | Azioni per ricaricare | Distanza minima |
| :--- | :---: | ---: |
| Fionde | 0 | Gioco largo |
| Archi | 0 | Gioco medio |
| Balestre | 2 | Gioco medio |
| Armi da lancio | 0 | Gioco medio |

#### Fionde

Le fionde sono pezzi di tela in cui si inserisce un sasso o proiettile, vengono fatte roteare, e scagliano il proiettile verso il nemico. Serve abbastanza spazio da far roteare la fionda, quindi non si possono usare se si ha un avversario in gioco stretto o medio (anche se non è il vostro bersaglio). Di contro, come i martelli, ignorano 2 punti dalle armature nemiche. Sassi e proiettili tendono a rompere le creature di carne, piuttosto che il contrario, quindi si possono sempre riusare, ma non è sempre facile ritrovarli dopo un combattimento.

#### Archi

Un bastone di legno flessibile e una corda. Non sono facili da usare, ma fanno un male cane e sono adatti per il tiro a campana (vedi sotto). Incoccare, tendere e mirare richiede un po' di concentrazione, quindi non potete prepararvi prima e andare in giro con la freccia incoccata.

##### Tiro a campana

Il tiro a campana, o tiro ballistico, consiste nel tirare verso l'alto per far ricadere le freccia sul nemico, aggirando tutte quelle cose fastidiose come mura, mobilia o barricate che vi impediscono di mirare alla faccia. E' un lavoro molto meno preciso, che funziona molto bene quando si è in parecchi a farlo e, soprattutto, richiede un po' di spazio sopra la testa: non si può fare al chiuso. In caso di dubbi, si può tirare Intelligenza + Combattimento per determinare se sussistono le condizioni per un tiro a campana. Se si decide di andare avanti, si tira due volte Destrezza + Combattimento. E' necessario che entrambi i tiri abbiano successo per mettere a segno il colpo.

#### Balestre

Un po' più semplici da usare degli archi, ma permettono solo il tiro diretto e sono molto lente a ricaricare. In compenso possono tenere dentro il quadrello per parecchio tempo, quindi si può girare preparati.

#### Armi da lancio

Giavellotti, pugnali da lancio, asce da lancio... Sono la versione leggermente ribilanciata dell'arma da mischia equivalente. Basta averle in mano per fare male. Se il vostro avversario è troppo vicino, potete usarle come un pugnale (pugnali, asce, stellette ninja) o come un bastone (giavellotti).

### Armature e scudi

Le armature si dividono in tre tipi: leggere, medie e pesanti. Avere indosso un'armatura è già sufficiente per godere della sua protezione, ma sono oggetti spesso ingombranti e poco confortevoli. Solo i Combattenti possono indossare armature pesanti.

Si possono indossare fino a quattro pezzi di armatura contemporaneamente, sommando i bonus per determinare la CA complessiva: gambe, braccia, torso e testa.

#### Armature leggere

Sono tipicamente fatte in tessuti imbottiti, con pochi rinforzi in cuoio. Forniscono un bonus di +1 per pezzo.


#### Armature medie

Sono in cuoio, con qualche rinforzo in metallo, o in maglia d'acciaio e un po' di imbottitura. Forniscono un bonus di +2 per pezzo.

#### Armature pesanti

Sono fatte in metallo, pieno o maglia, e tipicamente hanno un'imbottitura sotto per preservare l'utilizzatore da traumi. Forniscono un bonus di +3 per pezzo.


#### Scudi

Gli scudi possono essere fatti in legno o metallo, ma ciò che conta veramente è la loro forma: un buckler, anche se in metallo, è uno scudo leggero, mentre un grosso pavese in legno rimane uno scudo pesante. Tutti i personaggi possono usare uno scudo leggero, ma solo i Combattenti hanno l'addestramento per usare efficacemente quelli pesanti. Inoltre, indossare uno scudo su un braccio lasciato a penzoloni, non è sufficiente per godere della sua protezione: occorre usare un'azione per muovere attivamente lo scudo e cercare di difendersi. Gli scudi leggeri danno un bonus di +1 alla CA, quelli pesanti di +2. I Maghi possono usare solo i buckler, perché non sono un ingombro sufficiente a impedire di compiere i loro gesti arcani.
