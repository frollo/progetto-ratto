# Prefazione

## Cos'è Progetto Ratto

E' un gdr completamente open source. In qualsiasi modo abbiate ottenuto questo file, potete andare [qui](https://gitlab.com/frollo/progetto-ratto), recuperare i sorgenti, modificarli come volete, e ottenere il vostro sistema. Se sapete usare anche `git`, potete fare un vostro fork.

L'idea è di mettere assieme appunti, idee e simili che ho raccolto negli anni dicendomi "vorrei una roba del genere nel mio gioco" e farci il mio gioco. Anche se poi non lo gioco che io. Punto a mantenere lo spirito con cui ho raccolto il tutto, cioè come hack per altri sistemi. Idealmente, qualsiasi dei pezzi del regolamento può essere preso e attaccato su un gioco OSR, così come può essere sostituito dalla versione che preferite.
